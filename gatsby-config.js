/** @type {import('gatsby').GatsbyConfig} */
module.exports = {
  siteMetadata: {
      title: ``,
    siteUrl: `https://www.yourdomain.tld`
  },
  plugins: [
	"gatsby-plugin-styled-components", 
	{
		resolve: 'gatsby-plugin-google-analytics',
		options: {
		"trackingId": "G-HGBR0K0Q3K"
		}
	}, 
	"gatsby-plugin-image", 
	"gatsby-plugin-react-helmet", 
	"gatsby-plugin-sitemap", 
	"gatsby-plugin-sharp", 
	"gatsby-transformer-sharp",
	"gatsby-plugin-react-helmet",
	{
		resolve: 'gatsby-source-filesystem',
		options: {
		"name": "images",
		"path": "./src/images/"
	},
    __key: "images"
  }]
};